---
title: "Deployment Guide"
weight: 1
---
# Description
The goal of this challenge was to convert the standalone HTML prototype into a Hugo template. Below are instructions how to deploy the theme and validate the result.
# Install Hugo
Following guide if for macOS. You can find documentation for Windows and Linux at the https://gohugo.io/getting-started/installing/.

Install Hugo using Homebrew on macOS:
```bash
brew install hugo
```
To verify your new install:
```bash
hugo version
```

# Local Deployment
To test the website locally, please execute the following
```bash
hugo server
```
Now you can open the browser and see the website localy at http://localhost:1313/ (by default).

# Production Build and Installation
First of all, you should specify `baseURL` in the `config.toml`. For example:
```
baseURL = "https://example.com/"
```
Then, build static files using `hugo` command
```bash
hugo
```
It will create `public` folder with static files inside.
Upload these files to the production server.

# Notes
## Challange Scope
In the scope of this challenge were following 10 pages:
1. http://localhost:1313/ - Prototype/landing.html
2. http://localhost:1313/documentation/ - Prototype/documentation.html
3. http://localhost:1313/blog/ - Prototype/blog.html
4. http://localhost:1313/blog/post-1/ - Prototype/blog-details.html
5. http://localhost:1313/login/ - Prototype/login.html
6. http://localhost:1313/apis/ - Prototype/API-page.html
7. http://localhost:1313/dev-portal/ - Prototype/dev-portal.html
8. http://localhost:1313/profile/ - Prototype/profile.html
9. http://localhost:1313/issues/ - Prototype/application-page.html
10. http://localhost:1313/platform-status/ - Prototype/platform-status.html

## Hugo Theme
Created Hugo theme is located under the `/iris-portal/theme/iris` folder.

## Hugo Content Management
I have created .md files in the content management for every page (`/iris-portal/content/`). Blogs are located at `/iris-portal/content/blog/`. Also, I have moved JSON mock data for dynamic content to the `/iris-portal/data/`. I think it wasn't in the scope of the challenge, but it is nice to have and templates looks more clean.

Also, all website menus are created using Hugo Menu Templates and described in the `config.toml`.

## Teams Dropdown
As requested, teams drop down was converted and integrated on the dev portal pages (#7-#10).
NOTE: checkbox icons should be replaced with more quality images (it was out of scope):
1. /iris-portal/themes/iris/static/i/checkbox-checked.png
2. /iris-portal/themes/iris/static/i/checkbox-unchecked.png
