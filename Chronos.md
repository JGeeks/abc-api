---
title: "Chronos"
weight: 0
---
# Introduction
Chronos is distributed job scheduler/execution environment.  It is capable
of scheduling jobs with cron like schedules and executing them once, multiple
times or forever.  In addition it has immediate execution feature to run quick
lambda like functions.

All jobs/functions are programs that can be written in JavaScript, Go or Python.
Jobs run as Docker containers.  In order to create a job you need to have source
that provides Dockerfile to build it.  You then use Chronos tools to create
job along with the schedule.

# How to build Chronos
```
git clone git@github.com:iris-platform/chronos.git
cd chronos
export GOPATH=`pwd`
cd src/chronos/cmd/chronssvc
go build
./chronossvc --config config.json
```

# How to create a job
In order to create Chronos job your code needs to have a Dockerfile that will
build a Docker container with your code.  Once you have that you will need to do
the following to prepare your job for publishing it to Chronos:
* Create chronos.json file in the root of your project.  For your convenience
you may want to commit this file with your project.  You will need the following
information in chronos.json:

```
{
  "version": "1.0.0",
  "name": "chronostest",
  "schedule": "@hourly",
  "repeat": 0,
  "callback": "http://localhost:3007/jobcallback/uno",
  "check_in_threshold": 240
}
```

* version - this is a string identifying version of your job
* name - this is the name of your job and it will be also used as a name for the docker container
* schedule - this string is the schedule specification for your job.  It follows cron tab
format but it supports seconds granulity.  See Cron Expression Format section below.
* repeat - specifies how many times to repeat the job.  -1 means always repeat, 0 run once and do not repeat, 1 means repeat once, 2 repeat twice and so on.
* callback - when job is completed Chronos will execute POST on the specified URL to notify of job completion.
* check_in_threshold - currently not used but it will be required in the next release to specify duration between heartbeats from the job to Chronos.  This field must be present in the configuration file (chronos.json).

# CRON Expression Format
A cron expression represents a set of times, using 6 space-separated fields.
```
Field name   | Mandatory? | Allowed values  | Allowed special characters
----------   | ---------- | --------------  | --------------------------
Seconds      | Yes        | 0-59            | * / , -
Minutes      | Yes        | 0-59            | * / , -
Hours        | Yes        | 0-23            | * / , -
Day of month | Yes        | 1-31            | * / , - ?
Month        | Yes        | 1-12 or JAN-DEC | * / , -
Day of week  | Yes        | 0-6 or SUN-SAT  | * / , - ?
Note: Month and Day-of-week field values are case insensitive. "SUN", "Sun", and "sun" are equally accepted.
```
## Special Characters
Asterisk ( * )

The asterisk indicates that the cron expression will match for all values of the field; e.g., using an asterisk in the 5th field (month) would indicate every month.

Slash ( / )

Slashes are used to describe increments of ranges. For example 3-59/15 in the 1st field (minutes) would indicate the 3rd minute of the hour and every 15 minutes thereafter. The form "*\/..." is equivalent to the form "first-last/...", that is, an increment over the largest possible range of the field. The form "N/..." is accepted as meaning "N-MAX/...", that is, starting at N, use the increment until the end of that specific range. It does not wrap around.

Comma ( , )

Commas are used to separate items of a list. For example, using "MON,WED,FRI" in the 5th field (day of week) would mean Mondays, Wednesdays and Fridays.

Hyphen ( - )

Hyphens are used to define ranges. For example, 9-17 would indicate every hour between 9am and 5pm inclusive.

Question mark ( ? )

Question mark may be used instead of '*' for leaving either day-of-month or day-of-week blank.

## Predefined schedules
You may use one of several pre-defined schedules in place of a cron expression.
```
Entry                  | Description                                | Equivalent To
-----                  | -----------                                | -------------
@yearly (or @annually) | Run once a year, midnight, Jan. 1st        | 0 0 0 1 1 *
@monthly               | Run once a month, midnight, first of month | 0 0 0 1 * *
@weekly                | Run once a week, midnight on Sunday        | 0 0 0 * * 0
@daily (or @midnight)  | Run once a day, midnight                   | 0 0 0 * * *
@hourly                | Run once an hour, beginning of hour        | 0 0 * * * *
```
## Intervals
You may also schedule a job to execute at fixed intervals, starting at the time it's added or cron is run. This is supported by formatting the cron spec like this:
```
@every <duration>
```
where "duration" is a string of possibly signed sequence of decimal numbers, each with optional fraction and a unit suffix, such as "300ms", "-1.5h" or "2h45m". Valid time units are "ns", "us" (or "碌s"), "ms", "s", "m", "h".

For example, "@every 1h30m10s" would indicate a schedule that activates immediately, and then every 1 hour, 30 minutes, 10 seconds.

## Note
Chronos takes into consideration time it takes to run a job.  So each scheduled time
is calculated on initial scheduling and then recalculated after the job is finished.

# How to publish a job
To publish the job you must build your Docker container locally and name it like this:
```
st-docreg-asb-001.poc.sys.comcast.net/<job name as defined in chronos.json>
```

Here are sample steps:
* docker build -t st-docreg-asb-001.poc.sys.comcast.net/chronostest .
* chronoscli publish

# How to schedule a job
In order to schedule Chronos job you must complete previous steps as described above and be in the root folder of your job code where chronos.json is defined.

Also you will need to login to Chronos with chrons command line tools:
* chronoscli login - this command will ask you for your application key and secret.

* chronoscli schedule - this command will schedule your job and return uuid that is your job's ID.  You can use this ID with other commands to check on your job's status.

# How to check job status
You can check job status with the following command:
```
chronoscli jobinfo <jobID>
```

```
+--------------------------------------+--------------+-----------------+---------+----------+-----------------+----------------+-------------------------------+
|                  ID                  |     NAME     |      STATE      | STATUS  | SCHEDULE | TIMES TO REPEAT | TIMES EXECUTED |      NEXT SCHEDULED TIME      |
+--------------------------------------+--------------+-----------------+---------+----------+-----------------+----------------+-------------------------------+
| ace243a2-9555-4f23-a832-bdbc9ac87e97 | chronostest4 | Not schedulable | Success | @hourly  | Never Repeat    |              1 | 2017-07-19 08:00:00 -0400 EDT |
+--------------------------------------+--------------+-----------------+---------+----------+-----------------+----------------+-------------------------------+
```

Or check all jobs in your domain:
* chronoscli allinfo

```
+--------------------------------------+--------------+-----------------+---------+-----------+-----------------+----------------+-------------------------------+
|                  ID                  |     NAME     |      STATE      | STATUS  | SCHEDULE  | TIMES TO REPEAT | TIMES EXECUTED |      NEXT SCHEDULED TIME      |
+--------------------------------------+--------------+-----------------+---------+-----------+-----------------+----------------+-------------------------------+
| 4d0f30ed-30bf-4e7f-816e-e5805112f776 | chronostest3 | Not schedulable | Success | 0 * * * * | Never Repeat    |              1 | 2017-07-03 07:47:00 -0400 EDT |
| e37cd89b-bfb6-4b97-9095-25912c6cdacc | chronostest3 | Not schedulable | Success | 0 * * * * | Never Repeat    |              1 | 2017-07-03 16:27:00 -0400 EDT |
| 5838f4e8-4e5e-4acf-a82f-45aa34dd5443 | chronostest3 | Not schedulable | Success | 0 * * * * | Never Repeat    |              1 | 2017-07-03 18:32:00 -0400 EDT |
| a814cd89-0046-4fef-b7b5-b730355c4a92 | chronostest4 | Registered      | Not Run | @daily    | Never Repeat    |              0 | 2017-07-20 00:00:00 -0400 EDT |
| ebebe4c0-a6fd-4510-8d82-6a2892c9af62 | chronostest4 | Registered      | Not Run | @hourly   | Never Repeat    |              0 | 2017-07-19 08:00:00 -0400 EDT |
| ace243a2-9555-4f23-a832-bdbc9ac87e97 | chronostest4 | Registered      | Not Run | @hourly   | Never Repeat    |              0 | 2017-07-19 08:00:00 -0400 EDT |
+--------------------------------------+--------------+-----------------+---------+-----------+-----------------+----------------+-------------------------------+
```

# APIs
All of the APIs below require following headers:

```
Content-Type: application/json
Authorization: Bearer <iris server to server JWT>
```

## POST /v1/schedule
This API will schedule the job just like Chronos command line schedule command.

```
curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer: <JWT>"
-d '{"job_name": "Test job",
  "job_container_id": "st-docreg-asb-001.poc.sys.comcast.net/chronostest3",
  "schedule": "* * * * *",
  "repeat_job": 0,
  "callback": "http://localhost:3007/jobcallback/someparam",
  "check_in_threshold": 240}' https://https://st-chronos-asb-002.poc.sys.comcast.net/v1/schedule
```

Returns 200OK if successful:
```
{
  "job_id": "uuid of newly scheduled job"
}
```

Errors:
400 - if the payload is incorrect
401 - if the JWT is invalid
500 - if there is any Chronos problem

## GET /v1/jobinfo/{jobid}
Get the status of the job identified by jobid.  Job id is received from schedule
API.

```
curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer: <JWT>"
https://https://st-chronos-asb-002.poc.sys.comcast.net/v1/jobinfo/58207474-6d2c-4760-8ad6-dff481973c9b
```

Returns 200OK if successful:
```
{
    "job_id": "58207474-6d2c-4760-8ad6-dff481973c9b",
    "job_name": "roberto4",
    "job_container_id": "st-docreg-asb-001.poc.sys.comcast.net/roberto4",
    "job_instance_id": "58207474479999008ad6dff481909090",
    "schedule": "0 * * * *",
    "next_scheduled_run": 1500925500,
    "times_executed": 0,
    "callback": "http://localhost:3007/jobcallback/uno",
    "check_in_threshold": 240,
    "state": "registered",
    "status": "notrun",
    "status_description": ""
}
```

Errors:
400 - if the payload is incorrect
401 - if the JWT is invalid
500 - if there is any Chronos problem

## GET /v1/getargs/instanceid/{instanceid}
Get arguments of the job by instance ID.

```
curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer: <JWT>"
https://https://st-chronos-asb-002.poc.sys.comcast.net/v1/getargs/instanceid/99f18028ef7fce62c796e54a03746
```

Returns 200OK if successful

```
{
    "args": "{test_args: {arg1, arg2}"
}
```

Errors:
204 - if there are no arguments for the job with provided instance ID
400 - if the is no job with the provided instance ID
401 - if the JWT is invalid
500 - if there is any Chronos problem

## GET /v1/alljobsinfo/skip/{skip}/limit/{limit}/status/{status}/state/{state}
Returns all of the jobs for you app domain with pagination and filter on status
and state.

Status and state filter accepts wildcard * or the following values:

Status:
```
unknown
notrun
executing
executinghealthcheck
success
error
forcedstop
```

State:
```
unknown
stopped
registered
scheduled
scheduling
running
paused
notschedulable
```

```
curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer: <JWT>"
https://st-chronos-asb-002.poc.sys.comcast.net/v1/alljobsinfo/skip/0/limit/10/status/*/state/*
```

Returns 200OK if success.  Sample response:
```
{
    "skip": 0,
    "count": 7,
    "max": 7,
    "job_descriptors": [
        {
            "job_id": "58207474-6d2c-4760-8ad6-dff481973c9b",
            "job_name": "roberto4",
            "job_container_id": "st-docreg-asb-001.poc.sys.comcast.net/roberto4",
            "job_instance_id": "aa2729be8534327cede6cffe0290aa6ea444ceea07a6ea06e1d3a7399bd66d6c",
            "schedule": "0 * * * *",
            "next_scheduled_run": 1500925500,
            "times_executed": 0,
            "callback": "http://localhost:3007/jobcallback/uno",
            "check_in_threshold": 240,
            "state": "running",
            "status": "executinghealthcheck",
            "status_description": "Counter=7653"
        },
        {
            "job_id": "c6705096-00ea-434a-b2a4-563027624805",
            "job_name": "roberto5",
            "job_container_id": "st-docreg-asb-001.poc.sys.comcast.net/roberto5",
            "job_instance_id": "d28c87f969eaf86fc43fc83f2562b365b395673064fa7a336465f1f3ae133807",
            "schedule": "* * * * *",
            "next_scheduled_run": 1500986227,
            "times_executed": 1,
            "callback": "http://localhost:3007/jobcallback/uno",
            "check_in_threshold": 240,
            "state": "notschedulable",
            "status": "success",
            "status_description": "Counter=5695"
        },
        {
            "job_id": "11a042e2-4f07-4c87-9469-087014f5cde2",
            "job_name": "roberto5",
            "job_container_id": "st-docreg-asb-001.poc.sys.comcast.net/roberto5",
            "job_instance_id": "a05af51025a9bb268def1e7cf864e312599267392d73e50b8f13fa4a6167a544",
            "schedule": "* * * * *",
            "next_scheduled_run": 1500986319,
            "times_executed": 1,
            "callback": "http://localhost:3007/jobcallback/uno",
            "check_in_threshold": 240,
            "state": "notschedulable",
            "status": "success",
            "status_description": "Counter=4556"
        },
        {
            "job_id": "53b8ce83-48b1-4010-be47-0b38e4e28c78",
            "job_name": "roberto5",
            "job_container_id": "st-docreg-asb-001.poc.sys.comcast.net/roberto5",
            "job_instance_id": "55aecc78e8c41c4f3ed62a1259bdd16fe1a273bf98ba747872c887ac40b60672",
            "schedule": "* * * * *",
            "next_scheduled_run": 1500986320,
            "times_executed": 1,
            "callback": "http://localhost:3007/jobcallback/uno",
            "check_in_threshold": 240,
            "state": "notschedulable",
            "status": "success",
            "status_description": "Counter=6622"
        },
        {
            "job_id": "f1891d12-88f4-4a57-983a-09824f108e2b",
            "job_name": "roberto5",
            "job_container_id": "st-docreg-asb-001.poc.sys.comcast.net/roberto5",
            "job_instance_id": "60fd76dd11272f28034c088fa976a1090c0431cbf2e8dc306ed11c131016a80b",
            "schedule": "* * * * *",
            "next_scheduled_run": 1500986324,
            "times_executed": 1,
            "callback": "http://localhost:3007/jobcallback/uno",
            "check_in_threshold": 240,
            "state": "notschedulable",
            "status": "success",
            "status_description": "Counter=4461"
        },
        {
            "job_id": "23bbea2e-41b3-48a2-b107-17af8ff9cc18",
            "job_name": "roberto5",
            "job_container_id": "st-docreg-asb-001.poc.sys.comcast.net/roberto5",
            "job_instance_id": "33cba5209be66e1032609dab97859a6d00e4e06c090da70924e07c348e5a2d0d",
            "schedule": "* * * * *",
            "next_scheduled_run": 1500986325,
            "times_executed": 1,
            "callback": "http://localhost:3007/jobcallback/uno",
            "check_in_threshold": 240,
            "state": "notschedulable",
            "status": "success",
            "status_description": "Counter=6695"
        },
        {
            "job_id": "13da0a2d-9421-4c0f-960d-f5fbc99b3638",
            "job_name": "roberto5",
            "job_container_id": "st-docreg-asb-001.poc.sys.comcast.net/roberto5",
            "job_instance_id": "503331dfad90c21f1e13632528f31031ae0239b323a0bd1bb39984868a78bd5b",
            "schedule": "* * * * *",
            "next_scheduled_run": 1500986327,
            "times_executed": 1,
            "callback": "http://localhost:3007/jobcallback/uno",
            "check_in_threshold": 240,
            "state": "notschedulable",
            "status": "success",
            "status_description": "Counter=4595"
        }
    ]
}
```

Errors:
400 - if the payload is incorrect
401 - if the JWT is invalid
500 - if there is any Chronos problem

## POST /v1/jobcustomstatus
This API is used to update job status with custom information that job designer
may want to use.  The status is a string but you can pass strngified JSON to it
as well.

```
curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer: <JWT>"
-d '{
	"instance_id": "182c9fb78d643e55a77df09208c4a7153d30bf88941deb72a2b513015bc7cd61",
	"status": "Super cool!"
}' https://st-chronos-asb-002.poc.sys.comcast.net/v1/jobcustomstatus
```

Returns 200OK is success and empty object in response.

Errors:
400 - if the payload is incorrect
401 - if the JWT is invalid
500 - if there is any Chronos problem

## POST /v1/removejob
This API will remove job from schedule.  This API will only succeed if the job
is not running.

```
curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer: <JWT>"
-d '{
	"job_id": "778d9640-b860-4207-8feb-84bcedcf84c6"
}' https://st-chronos-asb-002.poc.sys.comcast.net/v1/jobcustomstatus
```

Returns 200OK is success and empty object in response.

Errors:
400 - if the payload is incorrect
401 - if the JWT is invalid
500 - if there is any Chronos problem

## GET /v1/version
Returns Chronos server version.

```
curl -X GET https://st-chronos-asb-002.poc.sys.comcast.net/v1/version
```

Sample output:
```
{"version":"Chronos Server 0.0.1"}
```